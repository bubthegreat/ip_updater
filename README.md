# Installation

```
git clone https://gitlab.com/bubthegreat/ip_updater.git
cd ip_updater
pip install .
```

# Usage

Show available providers.  Currently only godaddy.

```
(venv) PS C:\Users\bubth\Development\ip_updater> update_ip --show_providers
        godaddy

```

Update to a specific IP address

```
(venv) PS C:\Users\bubth\Development\ip_updater> update_ip bubtaylor.com --ip 1.2.3.4
Mismatched IP addresses found: 173.59.147.202 != 1.2.3.4
Updated IP address!
```

Update to the current IP address assigned by your ISP

```
(venv) PS C:\Users\bubth\Development\ip_updater> update_ip bubtaylor.com
Mismatched IP addresses found: 1.2.3.4 != 173.59.147.202
Updated IP address!
```