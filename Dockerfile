FROM python:3.7

WORKDIR /ip_updater
COPY . .
RUN rm -rf dist
RUN python setup.py sdist
RUN cd dist && pip install $(ls)
RUN rm -rf dist
RUN rm -rf build

RUN pytest -n auto --cov-report term-missing --cov-config=.coveragerc --cov=ip_updater tests
RUN mypy --config-file mypy.ini src
RUN pylint -E --rcfile=.pylintrc src/

ENTRYPOINT [ "update_ip" ]
