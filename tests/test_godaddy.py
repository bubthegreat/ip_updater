"""Generic passing test for build pipeline testing."""

import pytest
from ip_updater.providers.godaddy import GoDaddy

def test_godaddy_api():
    """Generic passing test for instantiating godaddy API."""
    assert GoDaddy('bubtaylor.com')

def test_fails_with_no_domain():
    with pytest.raises(TypeError):
        GoDaddy()  # pylint: disable=no-value-for-parameter