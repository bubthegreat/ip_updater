"""Base API layer for GoDaddy IP updater."""

import os

from godaddypy import Client, Account
from ip_updater.classes.api import UpdateAPI


class GoDaddy(UpdateAPI):

    REQUIRED_ENV_VARS = (
        'GODADDY_API_KEY',
        'GODADDY_API_SECRET'
    )

    def __init__(self, domain: str):
        """Create godaddy client."""
        super().__init__(domain)
        self.domain = domain
        self.client: Client = None

    def authenticate(self) -> bool:
        """Authenticate your API."""

        authenticated = False
        api_key = os.environ.get("GODADDY_API_KEY")
        api_secret = os.environ.get("GODADDY_API_SECRET")
        godaddy_account = Account(api_key=api_key, api_secret=api_secret)
        self.client = Client(godaddy_account)
        authenticated = True
        return authenticated

    def get_dns_ip(self, domain: str) -> str:
        records = self.client.get_records(domain, record_type='A')
        ip_addr = list(set([record['data'] for record in records]))
        if ip_addr and len(ip_addr) == 1:
            dns_ip: str = ip_addr[0]
        else:
            raise ValueError("Multiple IPs detected for domain!")
        return dns_ip

    def update_dns_ip(self, domain: str, ip_addr: str = None) -> bool:

        updated = False
        if self.client:
            updated = self.client.update_ip(ip_addr, domains=[domain])
        return updated