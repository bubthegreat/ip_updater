"""Main functions that are callable."""

import argparse
import sys
from ip_updater.providers.godaddy import GoDaddy

PROVIDERS = {
    'godaddy': GoDaddy
}

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('--provider', default='godaddy', help='Provider that should be used.')
    parser.add_argument('--ip', default=None, dest='ip_addr', help='IP Address that will be used to update.  Discovered if not provided from local source.')
    parser.add_argument('--show-providers', action='store_true', help='List available providers.')
    parser.add_argument('domain', nargs='?', help='Domain that will be updated.  Required.')
    args = parser.parse_args()
    return args


def show_providers() -> None:
    for provider_name in PROVIDERS:
        print(f'\t{provider_name}')


def validate_args(args: argparse.Namespace) -> bool:
    valid_args = True
    if args.provider not in PROVIDERS:
        print("Invalid provider selected.  Please select from the list:")
        show_providers()
        valid_args = False

    if args.domain is None:
        print("Please provide a valid domain")
        valid_args = False

    return valid_args


def main() -> None:
    args = parse_args()
    if args.show_providers:
        show_providers()
    else:
        valid_args = validate_args(args)

        if not valid_args:
            sys.exit(0)

        provider = PROVIDERS.get(args.provider)
        if provider:
            updater = provider(args.domain)
            updater.authenticate()
            updater.update_ips(ip_addr=args.ip_addr)