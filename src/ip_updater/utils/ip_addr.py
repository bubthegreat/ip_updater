"""General utils for IP address related functions."""

import requests
from typing import Optional

def get_my_ip() -> Optional[str]:
    result = requests.get('http://ipv4bot.whatismyipaddress.com')

    if result.status_code != 200:
        ip_addr = None
    else:
        ip_addr = result.text
    return ip_addr
