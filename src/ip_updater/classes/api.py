from abc import ABCMeta
from abc import abstractmethod
from typing import Iterable
from typing import Optional
from typing import List

from ip_updater.utils.ip_addr import get_my_ip


class UpdateAPI(metaclass=ABCMeta):

    REQUIRED_ENV_VARS: Iterable[str] = ()

    def __init__(self, domain: str):
        self.domain = domain
        self._my_ip: Optional[str] = None

    @property
    def my_ip(self) -> Optional[str]:
        if self._my_ip is None:
            self._my_ip = get_my_ip()
        return self._my_ip

    def validate(self) -> None:
        if not self.REQUIRED_ENV_VARS:
            raise AttributeError("Attribute REQUIRED_ENV_VARS cannot evaluate to False.")

    @abstractmethod
    def authenticate(self) -> bool:
        pass

    @abstractmethod
    def get_dns_ip(self, domain: str) -> str:
        pass

    @abstractmethod
    def update_dns_ip(self, domain: str, ip_addr: str = None) -> bool:
        pass

    def update_ips(self, ip_addr: str = None) -> bool:
        """Update the IP addresses on the DNS service."""

        updated = False
        dns_ip = self.get_dns_ip(self.domain)
        intended_ip = ip_addr or self.my_ip

        # If user provides an IP and it's different, update it.
        if ip_addr and dns_ip != intended_ip:
            print(f"Mismatched IP addresses found: {dns_ip} != {intended_ip}")
            updated = self.update_dns_ip(self.domain, intended_ip)

        # If use provided IP is the same, don't do nothin.
        elif ip_addr and dns_ip == intended_ip:
            print("DNS IP and given IP are the same, no need to change!")

        # If user doesn't provide an IP, check the ip from whatsmyip.com
        elif dns_ip != self.my_ip:
            print(f"Mismatched IP addresses found: {dns_ip} != {intended_ip}")
            updated = self.update_dns_ip(self.domain, intended_ip)

        # If we don't provide an IP address and the IP addresses aren't different
        # then don't change it.
        else:
            print(f"DNS IP and current IP are both {intended_ip}, no need to change!")

        if updated:
            print("Updated IP address!")

        return updated