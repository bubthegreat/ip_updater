#!/usr/bin/env python3

"""General setup functions for ip_updater."""

import json
import os
from setuptools import find_packages
from setuptools import setup

BASE_DIR = os.path.dirname(__file__)
SRC_DIR = os.path.join(BASE_DIR, 'src')
PACKAGE_BASE_DIR = os.path.join(SRC_DIR, 'ip_updater')
VERSION_FILE_NAME = 'version.json'
VERSION_FILE = os.path.join(PACKAGE_BASE_DIR, VERSION_FILE_NAME)


def get_version_dict():
    """Parse version json into version numbers."""
    with open(VERSION_FILE) as json_file:

        version_dict = json.loads(json_file.read())

    return version_dict


def save_version_dict(version_dict):
    """Parse version json into version numbers."""
    with open(VERSION_FILE, 'w') as json_file:
        json.dump(version_dict, json_file)


def parse_version_str(version_dict):

    major = version_dict['major']
    minor = version_dict['minor']
    maint = version_dict['maint']

    version_string = f"{major}.{minor}.{maint}"
    return version_string


def parse_version():
    version_dict = get_version_dict()
    version_str = parse_version_str(version_dict)
    return version_str

# Requirement info
REQUIREMENTS = [requirement for requirement in open('requirements.txt').readlines()]

ENTRY_POINTS = {
    'console_scripts': [
        'update_ip=ip_updater.main:main',
    ],
}

setup(
    name='ip_updater',
    version=parse_version(),
    description="IP Updater for ISPs that don't provide static IP addresses.",
    author="Micheal Taylor",
    author_email='bubthegreat@gmail.com',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points=ENTRY_POINTS,
    include_package_data=True,
    install_requires=REQUIREMENTS,
    python_requires='>=3.6.0',
)
